﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConstantsAndFormulas
{
    class Program
    {
        static void Main(string[] args)
        {
            ///List of variables
            string name = "0";
            double radius = 0.00;
            double length = 0.00;
            double height = 0.00;
            double pi = 3.14159265359;

            ///Beginning user interaction by having the user give their name
            Console.WriteLine("Hi there user, what's you name so I can personalise some geometric information for you?");
            name = (Console.ReadLine());
            Console.WriteLine($"Thank you {name}, I hope that you enjoy this exercise");

            ///Getting the user to give the numbers
            Console.WriteLine($"Alright {name}, I need you to give me a radius, length, and height in that order please");
            radius = double.Parse(Console.ReadLine());
            length = double.Parse(Console.ReadLine());
            height = double.Parse(Console.ReadLine());

            ///Response to the user input numbers
            Console.WriteLine("DID YOU KNOW?!");
            Console.WriteLine($"That a rectangle with length {length} and height {height} would have an area of {length * height}");
            Console.WriteLine($"and that the perimeter of that rectangle would be {2 * height + 2 * length}");
            Console.WriteLine($"That a circle with a radius {radius} would have an area of {radius * radius * pi}");
            Console.WriteLine($"and that the circle would have a circumference of {2 * radius * pi}");
            Console.WriteLine($"Did you know all that {name}? Did you? Did you know that {name}? I did!");
        }
    }
}
